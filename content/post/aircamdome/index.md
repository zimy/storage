---
title: "Ubiquiti AirCam Dome"
date: 2019-10-10T16:16:10+03:00
description: "Купольная камера видеонаблюдения 3000"
categories: ["sell"]
dropCap: true
displayInMenu: true
displayInList: true
draft: true
resources:
- name: featuredImage
  src: "AirVision_3_3.jpg"
  params:
    description: "Изображение камеры с сайта"
---
# 3000 рублей
Продаю потолочную камеру видеонаблюдения, подключающуюся по Ethernet. Мне не понравилась.
