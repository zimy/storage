---
title: "Peak Design Capture Clip v2"
date: 2019-10-09T20:00:23+03:00
description: "Клипса для подвески фототехники - 2000"
categories: ["rent","sell"]
dropCap: true
displayInMenu: true
displayInList: true
draft: true
resources:
- name: featuredImage
  src: 6133332396.jpg
  params:
    description: "Peak Design Capture clip v2"
---
# 2000 рублей
Держалку можно закрепить на пояс или лямку рюкзака. Ношу вместе с Lens Kit for Canon EF

Продаю, потому что хочу взять новое поколение системы Peak Design Capture.

Ссылка на систему на [сайте производителя](https://www.peakdesign.com/products/capture)

Ссылка на [объявление на Авито](https://www.avito.ru/moskva/fototehnika/peak_design_capture_clip_v2_1790210817)

