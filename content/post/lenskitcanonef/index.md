---
title: "Peak Design Lens Kit for Canon EF"
date: 2019-10-09T21:14:12+03:00
description: "Крепёж для объективов - 3500"
categories: ["rent", "sell"]
dropCap: true
displayInMenu: true
displayInList: true
draft: false
resources:
- name: featuredImage
  src: lenskitef.jpg
  params:
    description: "Крепёж для объективов"
---
# 3500 рублей
Держатель объективов, совместимый с клипсами Peak Design Capture clip второго и третьего поколений. Можно носить два объектива для камеры системы Canon EF на поясе или лямке рюкзака.

Ссылка на [сайт производителя](https://www.peakdesign.com/products/lens-kit?variant=10151318945836)

Ссылка на [моё объявление на Avito](https://www.avito.ru/moskva/fototehnika/peak_design_lens_kit_canon_ef_1790263013)
